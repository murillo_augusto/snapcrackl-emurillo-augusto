function snapCrackle(maxValue) {
  maxValue = prompt('Digite um valor');
  let arrCrackle = [];
  for (let i = 1; i <= maxValue; i++) {

    if (i % 5 === 0 && i % 2 === 1) {
      arrCrackle.push('SnapCrackle');
    } else if (i % 2 !== 0) {
      arrCrackle.push('Snap');
    } else if (i % 5 === 0) {
      arrCrackle.push('Crackle');
    } else if (i % 5 !== 0 && i % 2 !== 1) {
      arrCrackle.push(i);
    }
  }
  return console.log(arrCrackle.toString());
}

function snapCracklePrime(maxValue) {
  maxValue = prompt('Digite um valor');
  let arrCrackle = [];

  for (let i = 1; i <= maxValue; i++) {
    let primo = true;
    for (let z = 2; z < i; z++) {
      if (i % z === 0) {
        primo = false;
      }
    }
    if (i % 5 === 0 && i % 2 === 1 && primo === true) {
      arrCrackle.push(' SnapCracklePrime');
    } else if (i % 5 === 0 && i % 2 === 1) {
      arrCrackle.push(' SnapCrackle');
    } else if (i % 2 === 1 && primo === true && i !== 1) {
      arrCrackle.push('SnapPrime')
    } else if (i % 2 === 1) {
      arrCrackle.push(' Snap ');
    } else if (i % 5 === 0 && primo === true) {
      arrCrackle.push(' CracklePrime ');
    } else if (i % 5 === 0) {
      arrCrackle.push('Crackle');
    } else if (primo === true) {
      arrCrackle.push('Prime');
    } else {
      arrCrackle.push(i);
    }
  }

  return console.log(arrCrackle.toString());
}

